import React from 'react';
import styles from '../components/Task.module.css'

const Task = ({item, onChk, onDelete}) => {
    return (
    <li className = {styles.task}>
        {item.status === 'pending' ? <p>{item.name}</p> : <del>{item.name}</del>}
        <div  >
            <button className ={styles.chkBtn} onClick={()=>{onChk(item.id)} }>🗸</button>
            <button className ={styles.delBtn} onClick={()=>{onDelete(item.id)} }>🗑️</button>  
        </div>
     </li>
    );
}
 
export default Task;