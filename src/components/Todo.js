import React, { useState } from 'react';
import styles from './Todo.module.css';
import Task from '../components/Task';
const Todo = () => {
    // task list
    const storedTasks = JSON.parse(localStorage.getItem("tasks"));
    const initialTasks = storedTasks ? storedTasks : [];
    
    const [list, setList] = useState(initialTasks);

    // input text
    const[input, setInput] = useState("");

    // event handlers
    const inputChangeHandler = (e) => {
        
        setInput(e.target.value );
    }
     
    const addItemHandler = (e) => {
        e.preventDefault();
        if (input.trim() !== "") {
        const task = {
            id: Math.floor(Math.random() * 25484613),
            name: input,
            status: 'pending' 
        };
        const previousTasks = list;
        setList([...previousTasks, task]);
        setInput("");
        localStorage.setItem("tasks",JSON.stringify([...previousTasks, task]));
        }  
    }

    const clearAllHandler = () => {
        const isConfirmed = window.confirm("Are you sure you want to delete all tasks?");
        if(isConfirmed){
         setList([]);
         localStorage.setItem("tasks", JSON.stringify(""));   
        } 
    }
    
    const chkTaskHandler = (id) => {
        // console.log(id, 'done');
        const updatedList = list.map((element)=>{
            if(element.id === id)
            element.status = element.status === 'pending' ? 'completed' : 'pending';
            return element;
        });
        setList(updatedList);
        localStorage.setItem("tasks", JSON.stringify(updatedList));

    }
    
    const delTaskHandler = (id) => {
        const isConfirmed = window.confirm("Are you sure you want to delete this task?");
        
        if (isConfirmed) {
            const updatedList = list.filter((element) => {
               return (element.id !== id)});
            setList(updatedList);
            localStorage.setItem("tasks", JSON.stringify(updatedList));
        }
    }

        
    return ( 
    <div className={styles.mainContainer}>
        <div className={styles.textContainer}>
            <h1>Make your todo list</h1>
            <div>
                <p>Add your task here ✏️</p>
                <form>
                    <input type="text" placeholder = 'Your task' value={input} onChange={inputChangeHandler} />
                    <button onClick={addItemHandler} >Submit</button>
                </form>
            </div>
        </div>
        <h4>Your Tasks</h4>
        <ul>
            {list.map((element)=>{
                return<Task item={element} onChk={chkTaskHandler} onDelete = {delTaskHandler} />
            })}
        </ul>
        {list.length !== 0 && (<button className= {styles.clearBtn} onClick ={clearAllHandler} >Clear All</button>
        )}
        
    </div> 
    );
}
 
export default Todo;